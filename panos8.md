# Basic Setup
* Set "terminal length 0"

> set cli pager off

* Set Management IP address

> set deviceconfig system ip-address 1.2.3.4

> set deviceconfig system netmask 255.255.255.0

> set deviceconfig system default-gateway 1.2.3.254

> set deviceconfig system dns-setting servers primary 4.4.4.4

> set deviceconfig system dns-setting servers secondary 8.8.4.4


* Set Username

> set mgt-config users ngadimin permissions role-based superuser yes

> set mgt-config users ngadimin password

# Monitoring
* Monitor current system and session info

> show system info

> show system environmentals

> show session info

> show session all

> show system resources follow (like "top" command )

* Check Interface or SFP information

> show interface all

> show interface ethernet1/6

> show system state filter-pretty sys.s1.p6.phy

> show arp ethernet1/6

* Check connected Users

> show admins

# Troubleshooting
* Find certain command

> find command keyword <keyword>

* COnnectivity

> ping host <ipaddress/name> (use management interface)

> ping source <localipaddress> host <destipaddress>

> traceroute host <ipaddress/name>

* CHeck log (management or dataplane)

> tail follow yes {mp-log | dp-log} <logname.log>

* CHeck counter. Or use panchrome to get hourly statistics

> show counter global | match flow_tcp

# High Availability
* Check session sync in both FW

> show high-availability state-synchronization

* Trigger HA failover

> request high-availability state suspend

> request high-availability state functional

> request high-availability state peer suspend

> request high-availability state peer functional

* Other HA related command

> show high-availability ?

> show high-availability all

> show high-availability state

> show high-availability link-monitoring

> show high-availability path-monitoring

> show high-availability control-link statistics

> show high-availability state-synchronization

# Update signature/db. Assuming box is not connected to Internet
* Download from support page > PAN > Dynamic updates
* Upload to fw. For URL database only works for BrightCloud not PANDB

> scp import signed-url-database from user@1.2.3.4:/home/user/bcdb_3_660.enc

> request url-filtering install signed-database


# Misc
* Show set command

> set cli config-output-format set

> configure

> show
